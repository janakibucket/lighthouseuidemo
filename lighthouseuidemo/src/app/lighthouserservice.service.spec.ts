import { TestBed } from '@angular/core/testing';

import { LighthouserserviceService } from './lighthouserservice.service';

describe('LighthouserserviceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LighthouserserviceService = TestBed.get(LighthouserserviceService);
    expect(service).toBeTruthy();
  });
});
