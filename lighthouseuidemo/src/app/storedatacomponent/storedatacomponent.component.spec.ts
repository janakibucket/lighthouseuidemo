import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StoredatacomponentComponent } from './storedatacomponent.component';

describe('StoredatacomponentComponent', () => {
  let component: StoredatacomponentComponent;
  let fixture: ComponentFixture<StoredatacomponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StoredatacomponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StoredatacomponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
