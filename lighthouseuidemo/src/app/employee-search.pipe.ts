import { Pipe, PipeTransform } from '@angular/core';
import { Department } from './model/department';
import { Employee } from './model/employee';

@Pipe({
  name: 'employeeSearch'
})
export class EmployeeSearchPipe implements PipeTransform {

  transform(employees: Employee[], args?: any): any {
    return employees.filter(emp => emp.empName != undefined && args!= undefined  ? emp.empName.toLowerCase().indexOf(args.toLowerCase()) !== -1:true);
  }

}
