import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient,HttpClientModule  } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LighthouserserviceService {
  url = "http://localhost:8888/";
  constructor(private http: HttpClient) { }

  getEmployeesByDept(): Observable<any> {
    let getUrl = this.url + "/lighthouse/departments";
    return this.http.get(getUrl);
  }
}
