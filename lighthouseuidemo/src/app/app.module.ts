import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { HttpClient,HttpClientModule  } from '@angular/common/http';
import { AppComponent } from './app.component';
import { HomecomponentComponent } from './homecomponent/homecomponent.component';
import { DashboardcomponentComponent } from './dashboardcomponent/dashboardcomponent.component';
import { StoredatacomponentComponent } from './storedatacomponent/storedatacomponent.component';
import { EmployeeSearchPipe } from './employee-search.pipe';

@NgModule({
  declarations: [
    AppComponent,
    HomecomponentComponent,
    DashboardcomponentComponent,
    StoredatacomponentComponent,
    EmployeeSearchPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule 
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
