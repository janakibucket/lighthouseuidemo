import { Component, OnInit } from '@angular/core';
import { Department } from '../model/department';
import { LighthouserserviceService } from '../lighthouserservice.service';
import { filter } from 'rxjs/operators';
import { EmployeeSearchPipe } from '../employee-search.pipe';

@Component({
  selector: 'app-dashboardcomponent',
  templateUrl: './dashboardcomponent.component.html',
  styleUrls: ['./dashboardcomponent.component.css']
})
export class DashboardcomponentComponent implements OnInit {
  
  departments:Department[];
  searchEmp : any;

  constructor(private lhService:LighthouserserviceService) { }

  ngOnInit() {
    this.getEmployeeData();
  }
  getEmployeeData(){
    this.lhService.getEmployeesByDept().
    subscribe((data:Department[]) => {
      //debugger;
        if(data != null && data != undefined){
            this.departments = data;
        }
    });
  }
}
