import { Employee } from './employee';

export class Department {
    deptId:number;
    deptName:string;
    employeeMOS:Employee[];
}
