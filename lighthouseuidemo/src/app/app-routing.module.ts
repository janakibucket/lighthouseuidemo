import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardcomponentComponent } from './dashboardcomponent/dashboardcomponent.component';
import { StoredatacomponentComponent } from './storedatacomponent/storedatacomponent.component';


const routes: Routes = [
  {path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  {path: 'dashboard', component: DashboardcomponentComponent},
  {path: 'storeData', component: StoredatacomponentComponent}
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
